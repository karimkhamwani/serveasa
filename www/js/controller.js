angular.module('starter.controllers', [])


//Splash Screen  in controller
.controller('SplashscreenController', function($scope,$http,$ionicLoading,$ionicHistory){

  $scope.data = {};
  $scope.data.bgColors = [];
  $scope.data.currentPage = 0;
  $scope.demostatus = "block";

  for (var i = 0; i < 2; i++) {
    $scope.data.bgColors.push("bgColor_" + i);
  }

  var setupSlider = function() {
    //some options to pass to our slider
    $scope.data.sliderOptions = {
      initialSlide: 0,
      direction: 'horizontal', //or vertical
      speed: 300,//0.3s transition
      pagination: false
    };

    //create delegate reference to link with slider
    $scope.data.sliderDelegate = null;

    //watch our sliderDelegate reference, and use it when it becomes available
    $scope.$watch('data.sliderDelegate', function(newVal, oldVal) {
      if (newVal != null) {
        $scope.data.sliderDelegate.on('slideChangeStart', function() {
          $scope.data.currentPage = $scope.data.sliderDelegate.activeIndex;
          console.log($scope.data.sliderDelegate.activeIndex)
          //use $scope.$apply() to refresh any content external to the slider
          if($scope.data.sliderDelegate.activeIndex === 1){
          	$scope.demostatus="none"
          	$scope.languagestatus ="block"
          }
          if($scope.data.sliderDelegate.activeIndex === 0){
          		$scope.demostatus="block"
          		$scope.languagestatus ="none"
          }
          $scope.$apply();
        });
      }
    });
  };

  setupSlider();
})




//Login controller
.controller('LoginController', function($scope,myService,$http,$location,$state){
  

})

//Sign up controller
.controller('SignupController', function($scope,$http,$ionicLoading,$ionicHistory){
	//goback
	$scope.myGoBack = function(){
		console.log("Hey")
		$ionicHistory.goBack();
	}
   
})



//Sign in controller
.controller('SigninController', function($scope,$http,$ionicLoading,$ionicHistory){
   
    //goback
	$scope.myGoBack = function(){
		console.log("Hey")
		$ionicHistory.goBack();
	}
})



//forget password controller
.controller('ForgetpasswordController', function($scope,$http,$ionicLoading,$ionicHistory){

	//goback
	$scope.myGoBack = function(){
		console.log("Hey")
		$ionicHistory.goBack();
	}
})



.controller('MenuController', function($scope,$http,$ionicLoading,$ionicHistory,$ionicSideMenuDelegate){
  $scope.toggleLeft = function() {
    $ionicSideMenuDelegate.toggleLeft();
  };



})




//Login controller
.controller('AppController', function($scope,myService,$http,$location,$state,$ionicSideMenuDelegate){
  $scope.toggleLeft = function() {
                $ionicSideMenuDelegate.toggleLeft();
                console.log("hey")


            };
  $scope.title = "Home"



})

.controller('searchController', function($scope,$http,$ionicLoading,$ionicHistory){
    $scope.$parent.title = "Search"


})


.controller('plumbingController', function($scope,$http,$ionicLoading,$ionicHistory,$state){
   console.log("plumbingController")



   //watch state of app if state changes rename title
   $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
    console.log("State changed: ", toState);
    if(toState.name == "app.home"){
      $scope.$parent.title = "Home"
    }
    else if (toState.name == "app.search"){
      $scope.$parent.title = "Search"
    }
    else if (toState.name == "app.plumbing"){
      $scope.$parent.title = "Plumbing"
    }

    $scope.go_to_vendorlist = function(){
      $scope.$parent.title = "ELECTRICIAN"
    }


  })





})

.controller('homeController', function($scope,$http,$ionicLoading,$ionicHistory,$rootScope){

      $scope.$parent.title = "Home"
     $scope.go_to_plumbing = function(){
       $scope.$parent.title = "Plumbing"
  }

})


.controller('vendorlistController', function($scope,$http,$ionicLoading,$ionicHistory){
    $scope.$parent.title = "ELECTRICIAN"
    console.log("Vendor loaded")


})



